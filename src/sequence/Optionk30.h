#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <vector>
#include "unistd.h"
#include "kmer.h"
#include "math.h"
#include <iostream>
#include <fstream>
#include <tuple>
#include <sdsl/int_vector_buffer.hpp>
#include <sdsl/wt_int.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/int_vector.hpp>
#include <cuckoohash_map.hh>
#define NUM_BITS 4
#define NUM_BITS_FRECS 16
#define NUM_BITS_FRECS_UNIQUE 18
#define POSITION_WRONG 99999
using namespace sdsl;

struct IndexChunk
{
    IndexChunk():
            hi(0), low(0) {}
    IndexChunk(const IndexChunk& other):
            hi(other.hi), low(other.low) {}
    IndexChunk(IndexChunk&& other):
            hi(other.hi), low(other.low) {}
    IndexChunk& operator=(const IndexChunk& other)
    {
        hi = other.hi;
        low = other.low;
        return *this;
    }

    size_t get() const
    {
        return ((size_t)hi << 32) + (size_t)low;
    }
    void set(size_t val)
    {
        low = val & ((1ULL << 32) - 1);
        hi = val >> 32;
    }

    uint8_t hi;
    uint32_t low;
} __attribute__((packed));
static_assert(sizeof(IndexChunk) == 5,
              "Unexpected size of IndexChunk structure");

struct ReadPosition
{
    ReadPosition(FastaRecord::Id readId = FastaRecord::ID_NONE,
                 int32_t position = 0):
            readId(readId), position(position) {}
    ReadPosition& operator=(const ReadPosition& rp){
        this->readId = rp.readId;
        this->position = rp.position;
        return *this;
    }
    FastaRecord::Id readId;
    int32_t position;
};

struct Read_Pos
{
    Read_Pos():size(0),data(nullptr){}
    Read_Pos(IndexChunk * rp):size(0),data(rp),frec(0){}
    Read_Pos(IndexChunk * rp,size_t frec):size(0),data(rp), frec(frec){}
    Read_Pos(size_t frec):size(0), frec(frec){}
    uint32_t size, frec;
    IndexChunk* data;
};

class Control{
public:
    //Little
    virtual void insert_alter(Kmer,bool){};
    virtual std::chrono::duration<double> update_frec(Kmer) {};
    virtual size_t size() {return 0;};
    virtual size_t size(int) {return 1;};
    virtual void show(){}
    /*
     * New
     */
    virtual size_t size_total() {return 1;};
    virtual size_t size_total_chunk(size_t) {return 1;};
    virtual void internal_count(Kmer) {};
    virtual size_t is_unique(size_t, IndexChunk*, size_t pos = 0) {return 1;};
    virtual size_t get_frec_solid(size_t, size_t pos = 99999){return 1;};

    virtual unsigned int get_sumTotal(){return 0;};
    virtual uint32_t getfrec( Kmer, size_t){return -1;};
    virtual uint32_t getfrecIn(size_t){return -1;};
    virtual unsigned int insert(Kmer) {return 0;};
    virtual int locate(Kmer) {return 0;};
    virtual int check(Kmer,bool = false) {return 0;};
    virtual std::pair<int, std::pair<size_t, size_t>> check_optimal(Kmer){return {0,{0,0}};}
    virtual void setChunckSize(){};
    virtual void mod_frec_pos(Kmer,IndexChunk*){};
    virtual void update_read_pos(Kmer, FastaRecord::Id,size_t){};
    virtual Read_Pos * get_pos(Kmer,size_t){return nullptr;};
    virtual void set_to_zero(Kmer){};
    //Big
    virtual std::chrono::duration<double> insert(Kmer,bool) {};
    virtual void upgrade(bool) {};
    virtual void update(Kmer) {};
    virtual uint32_t get_frec_int(int,int){return 0;}
    virtual Kmer return_kmer(int){return Kmer(0);};
    virtual Kmer return_kmer(int,int){return Kmer(0);};
    virtual void update(bool){};
    virtual bool what(){return false;};
    virtual void erase_frec(uint32_t,std::vector<uint32_t>){};
    virtual void FinalStep(Kmer,size_t){};
    virtual void sortIndexChunks(){};

    virtual std::pair<size_t,IndexChunk *> get_kmer_reads(Kmer,size_t){
        return std::pair<size_t,IndexChunk *>(0, nullptr);
    };
    //Destroyer
    virtual void clear(){};
};

class BitMap: public Control
{
public:
    BitMap();
    BitMap(int,size_t);
    ~BitMap()
    {}
    void mod_frec_pos(Kmer,IndexChunk*);
    void update_read_pos(Kmer,FastaRecord::Id,size_t);
    void insert_alter(Kmer,bool);
    std::chrono::duration<double> update_frec(Kmer);
    void upgrade(bool);
    size_t size();
    Read_Pos * get_pos(Kmer,size_t);
    unsigned int get_sumTotal();
    uint32_t getfrec( Kmer, size_t);
    uint32_t getfrecIn(size_t);
    unsigned int insert(Kmer kmer);
    int locate(Kmer kmer);
    std::pair<size_t,IndexChunk *> get_kmer_reads(Kmer,size_t);
    int check(Kmer,bool);
    std::pair<int, std::pair<size_t, size_t>> check_optimal(Kmer);
    void setChunckSize();
    Kmer return_kmer(int);
    void update(bool);
    bool what(){
        return true;
    }
    void set_to_zero(Kmer);
    void sortIndexChunks()
    {
        for (auto rp : read_pos)
        {
            std::sort(rp.data, rp.data + rp.size,
                      [](const IndexChunk& p1, const IndexChunk& p2)
                      {return p1.get() < p2.get();});
        }
    }

    void clear();
private:
    unsigned int _sumTotal = 0;
    void _insert_alter(Kmer kmer, bool){
        size_t kmer_repr = kmer.getRepresentation();
        /*if (kmer_repr > b_2[0].size())
            Logger::get().debug() << "Kmer: "<<kmer_repr<<" Size: "<<b_2[0].size();*/
        if (b[kmer_repr] == 1)
            return;
        if (Parameters::get().method == 1)
        {
            for (size_t i = 0; i < b_2.size(); i++)
                if (!b_2[i][kmer_repr]) {
                    b_2[i][kmer_repr] = 1;
                    if (i == (b_2.size() - 1)) {
                        b[kmer_repr] = 1;
                        _sumTotal++;
                    }
                    return;
                }
            return;
        } else if (Parameters::get().method == 2)
        {
            size_t offset = (_hardthreshold < 2)?_hardthreshold-1:_hardthreshold-1;
            v_3_bv[kmer_repr] = (v_3_bv[kmer_repr] << 1) | 1;
            if ((v_3_bv[kmer_repr] & (1 << (offset))) != 0)
            {
                b[kmer_repr] = 1;
                _sumTotal++;
            }
        } else if (Parameters::get().method == 4)
        {
            if (++v_3_bv[kmer_repr] == _hardthreshold)
            {
                b[kmer_repr] = 1;
                _sumTotal++;
            }
        } else if (Parameters::get().method == 3){
            if (++v_4_bv[kmer_repr] == _hardthreshold)
            {
                b[kmer_repr] = 1;
                _sumTotal++;
            }
        }
    }
    size_t maximum = 0;
    bit_vector b,_b_final;
    rank_support_v<1> b_rank,b_rank_tmp;
    select_support_mcl<1> b_select;
    std::vector<uint16_t> frec;
    std::vector<Read_Pos> read_pos;
    std::vector<bit_vector> b_2;
    int_vector<> v_3_bv;
    unsigned char * v_4_bv;
    int count = 0;
    size_t genome_size = pow(2,30), _hardthreshold;
};

class BitMapBig: public Control
{
public:
    BitMapBig(int,size_t);
    void mod_frec_pos(Kmer,IndexChunk*);
    void update_read_pos(Kmer,FastaRecord::Id,size_t);
    size_t size();
    size_t size(int);
    size_t size_total() {
        return b_rank_tmp(genome_size);
    }
    size_t size_total_chunk(size_t chunck)
    {
        chunck = b_rank_uniques(chunck);
        return _info_new_app[chunck].second;
    }
    void internal_count(Kmer kmer)
    {
        size_t place = b_rank_tmp(kmer.hash() % (size_t) genome_size);
        if (_not_unique[place] == 1)
        {
            _info_new_app[place].first += _block;
            _keys_not_unique[place] = (size_t*)realloc(_keys_not_unique[place]
                    ,_info_new_app[place].first*sizeof(size_t));
            _not_unique_read_pos[place] = (Read_Pos*)realloc(_not_unique_read_pos[place]
                    ,_info_new_app[place].first*sizeof(Read_Pos));
        }
    }
    size_t is_unique(size_t place, IndexChunk* rp, size_t pos)
    {
        if (_not_unique[place] == 0) {
            place -= b_rank_uniques(place);
            _unique_read_pos[place].data = rp;
            return 0;
        }
        place = b_rank_uniques(place);
        _not_unique_read_pos[place][pos].data = rp;
        return place;
    }
    size_t get_frec_solid(size_t place, size_t pos)
    {
        if (_not_unique[place] == 0)
        {
            place -= b_rank_uniques(place);
            return _unique_read_pos[place].frec;
        }
        if (pos != 99999) {
            place = b_rank_uniques(place);
            return _not_unique_read_pos[place][pos].frec;
        }
        return 0;
    }
    void show();
    unsigned int get_sumTotal();
    std::chrono::duration<double> insert(Kmer,bool);
    void upgrade(bool);
    void update(Kmer);
    Read_Pos * get_pos(Kmer,size_t);
    std::chrono::duration<double> _insert_2(Kmer);
    int check(Kmer,bool);
    std::pair<int, std::pair<size_t, size_t>> check_optimal(Kmer);
    uint32_t get_frec_int(int, int);
    uint32_t getfrec(Kmer,size_t);
    std::pair<size_t,IndexChunk *> get_kmer_reads(Kmer,size_t);
    Kmer return_kmer(int,int);
    void update(bool);
    bool what(){return false;}
    void clear();
    void erase_frec(uint32_t,std::vector<uint32_t>);
    void FinalStep(Kmer,size_t);
    void sortIndexChunks()
    {
        for (auto rp : _unique_read_pos)
        {
            std::sort(rp.data, rp.data + rp.size,
                      [](const IndexChunk& p1, const IndexChunk& p2)
                      {return p1.get() < p2.get();});
        }
        for (size_t i = 0; i < _not_unique_read_pos.size(); ++i)
        {
            for (size_t j = 0; j < _info_new_app[j].second; ++j)
                std::sort(_not_unique_read_pos[i][j].data, _not_unique_read_pos[i][j].data+_not_unique_read_pos[i][j].size,
                          [](const IndexChunk& p1, const IndexChunk& p2)
                          {return p1.get() < p2.get();});
        }
    }
private:
    void _initialize_frecs(size_t place, uint32_t frec){
        size_t limit_l = _info_frecs[place].second, limit_h = _info_frecs[place].first;
        for (size_t i = limit_l; i < limit_h; i++)
            _frecs[place][i] = frec;
    }
    void _insert_1_2(Kmer kmer){
        size_t place = kmer.hash() % (size_t) genome_size;
        if (b[place] == 1)
            return;
        if (Parameters::get().method == 1)
        {
            if (++bloom[place] == _h_threshold) {
                b[place] = 1;
                _sumsup++;
            }
        } else if (Parameters::get().method == 2)
        {
            size_t offset = (_h_threshold < 2)?_h_threshold-1:_h_threshold-2;
            if ((bloom[place] & (1 << (offset))) != 0)
            {
                b[place] = 1;
                _sumsup++;
            }
            bloom[place] = ((bloom[place] << 1) | 1);
        } else if (Parameters::get().method == 3)
        {
            if (++bloom_v3[place] == _h_threshold) {
                b[place] = 1;
                _sumsup++;
            }
            return;
        } else if (Parameters::get().method == 4)
        {
            for (size_t i = 0; i < b_2.size(); i++)
                if (!b_2[i][place]) {
                    b_2[i][place] = 1;
                    if (i == (b_2.size() - 1)) {
                        b[place] = 1;
                        _sumsup++;
                    }
                    return;
                }
            return;
        }
    }
    void _update_2(){
        printf("Not supported yet\n");
    }
    unsigned int _sumsup = 0, _cont_local = 0, _cuenta_interna,_cuckoos_count = 0;
    size_t maximum = 0;
    size_t _h_threshold;
    bool _big_genome = false;
    rank_support_v<1> b_rank,b_rank_tmp,b_rank_uniques;
    bit_vector b,_b_final;
    //New Approach
    int_vector<> bloom;
    unsigned char * bloom_v3;
    bit_vector _uniques,_not_unique, _uniques_frec, _non_uniques_frec;
    bit_vector _check_bit_vector;
    std::vector<size_t> _uniques_keys;
    std::vector<Read_Pos> _unique_read_pos;
    std::vector<size_t *> _keys_not_unique;
    std::vector<Read_Pos *> _not_unique_read_pos;
    std::vector<bit_vector> b_2;
    size_t genome_size = pow(2,30);
    const size_t default_genome_size = pow(2,30);
    const size_t max_freq_allowed = pow(2, NUM_BITS_FRECS)-1, max_freq_allowed_unique = pow(2, NUM_BITS_FRECS_UNIQUE)-1;

    //Test arrays
    std::vector<size_t> _keys_unique;
    //std::vector<uint16_t> _frec_unique;
    int_vector<18> _frec_unique;
    std::vector<uint16_t *> _frecs;
    std::vector<size_t *> _keys;
    std::vector<std::pair<uint16_t,uint16_t>> _info_frecs;
    std::vector<std::pair<uint8_t,uint8_t>> _info_new_app;
    uint8_t _block = 1;

    //Testing
    size_t num_ceros = 0, num_m_cero = 0, num_menos_1 = 0;
    size_t num_searches = 0;
};